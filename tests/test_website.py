import urllib.request
import pytest
import unittest

class test_website(unittest.TestCase):
        def test_read1(self):
            webUrl = urllib.request.urlopen('https://the-internet.herokuapp.com/context_menu')
            data = str(webUrl.read().decode("utf-8"))
            word1 = "Right-click in the box below to see one called 'the-internet'"
            flag=False
            if (word1 in data):
                pass
            else:
                self.fail("Not found")

         
        def test_read2(self):
            webUrl = urllib.request.urlopen('https://the-internet.herokuapp.com/context_menu')
            data = str(webUrl.read().decode("utf-8"))
            word2 = "Alibaba"
            flag=False
            if (word2 in data):
                 pass
            else:
                self.fail("Not found")

if __name__ == '__main__':
    unittest.main()
